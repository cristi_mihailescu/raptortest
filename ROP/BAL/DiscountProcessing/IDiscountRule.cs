﻿using ROP.DAL;

namespace ROP.BAL
{
    public interface IDiscountRule
    {
        decimal CalculateDiscount(CustomerEntity customer, OrderEntity order);
    }
}
