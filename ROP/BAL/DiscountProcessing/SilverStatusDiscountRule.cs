﻿using ROP.DAL;

namespace ROP.BAL
{
    public class SilverStatusDiscountRule : IDiscountRule
    {
        public decimal CalculateDiscount(CustomerEntity customer, OrderEntity order)
        {
            if (customer.Status == CustomerStatus.Silver)
            {
                return 0.1M * order.Total;
            }

            return 0.0M;       
        }
    }
}
