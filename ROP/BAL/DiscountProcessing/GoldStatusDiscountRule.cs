﻿using ROP.DAL;

namespace ROP.BAL
{
    public class GoldStatusDiscountRule : IDiscountRule
    {
        public decimal CalculateDiscount(CustomerEntity customer, OrderEntity order)
        {
            if (customer.Status == CustomerStatus.Gold)
            {
                return 0.15M * order.Total;
            }

            return 0.0M;
        }
    }
}
