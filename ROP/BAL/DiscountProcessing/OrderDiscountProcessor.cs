﻿using System;
using System.Collections.Generic;
using ROP.DAL;

namespace ROP.BAL
{
    public class OrderDiscountProcessor : IOrderDiscountProcessor
    {
        public void ApplyDiscount(CustomerEntity customer, OrderEntity order)
        {
            var silverStatusDiscountRule = new SilverStatusDiscountRule();
            var goldStautsDiscountRule = new GoldStatusDiscountRule();
            var rulesList = new List<IDiscountRule>()
            {
                silverStatusDiscountRule,
                goldStautsDiscountRule
            };

            Decimal discount = 0.0M;
            foreach (var discountRule in rulesList)
            {
                discount += discountRule.CalculateDiscount(customer, order);
            }

            order.Total -= discount;
            order.Discount = discount;
        }
    }
}
