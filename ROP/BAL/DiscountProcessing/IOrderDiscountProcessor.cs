﻿using ROP.DAL;

namespace ROP.BAL
{
    public interface IOrderDiscountProcessor
    {
        void ApplyDiscount(CustomerEntity customer, OrderEntity order);
    }
}
