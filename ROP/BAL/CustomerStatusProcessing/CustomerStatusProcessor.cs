﻿using ROP.DAL;

namespace ROP.BAL
{
    public class CustomerStatusProcessor : ICustomerStatusProcessor
    {
        public void ProcessCustomerStatus(CustomerEntity customer)
        {
            IStatusUpgrader goldStatusUpgrader = new GoldStatusUpgrader();
            IStatusUpgrader silverStatusUpgrader = new SilverStatusUpgrader(goldStatusUpgrader);

            silverStatusUpgrader.CanBeUpgraded(customer);
        }
    }
}
