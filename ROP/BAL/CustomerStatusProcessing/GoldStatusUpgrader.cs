﻿using System;
using System.Linq;
using ROP.DAL;

namespace ROP.BAL
{
    public class GoldStatusUpgrader : IStatusUpgrader
    {
        private readonly IStatusUpgrader _nextStatusUpgrader;

        public GoldStatusUpgrader(IStatusUpgrader nextStatusUpgrader)
        {
            _nextStatusUpgrader = nextStatusUpgrader;
        }

        public GoldStatusUpgrader() 
            : this(null)
        {
        }
        
        public void CanBeUpgraded(CustomerEntity customer)
        {
            if (customer.Status == CustomerStatus.Silver)
            {
                if (customer.Orders.Sum(o => o.Total) > 600M
                    && (DateTime.UtcNow - customer.LastStatusUpdate).TotalDays > 7.0)
                {
                    customer.Status = CustomerStatus.Gold;
                }
            } else
            {
                _nextStatusUpgrader?.CanBeUpgraded(customer);
            }
        }
    }
}
