﻿using System;
using System.Linq;
using ROP.DAL;

namespace ROP.BAL
{
    public class SilverStatusUpgrader : IStatusUpgrader
    {
        private readonly IStatusUpgrader _nextStatusUpgrader;

        public SilverStatusUpgrader(IStatusUpgrader nextStatusUpgrader)
        {
            _nextStatusUpgrader = nextStatusUpgrader;
        }

        public void CanBeUpgraded(CustomerEntity customer)
        {
            if (customer.Status == CustomerStatus.Regular)
            {
                if (customer.Orders.Sum(o => o.Total) > 300M
                    && customer.Orders.Any(o => (DateTime.UtcNow - o.PlacementDate).TotalDays > 30.0))
                {
                    customer.Status = CustomerStatus.Silver;
                }
            }
            else
            {
                _nextStatusUpgrader?.CanBeUpgraded(customer);
            }
        }
    }
}
