﻿using ROP.DAL;

namespace ROP.BAL
{
    public interface IStatusUpgrader
    {
        void CanBeUpgraded(CustomerEntity customer);
    }
}
