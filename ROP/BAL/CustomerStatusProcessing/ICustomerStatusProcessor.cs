﻿using ROP.DAL;

namespace ROP.BAL
{
    public interface ICustomerStatusProcessor
    {
        void ProcessCustomerStatus(CustomerEntity customer);
    }
}
