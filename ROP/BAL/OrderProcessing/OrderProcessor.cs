﻿using System.Collections.Generic;
using ROP.DAL;

namespace ROP.BAL
{
    public class OrderProcessor : IOrderProcessor
    {
        private readonly IRepository<CustomerEntity> _customerRepository;
        private readonly ICustomerStatusProcessor _customerStatusProcessor;
        private readonly IOrderDiscountProcessor _orderDiscountProcessor;

        public OrderProcessor(IRepository<CustomerEntity> customerRepository,
            ICustomerStatusProcessor customerStatusProcessor,
            IOrderDiscountProcessor orderDiscountProcessor)
        {
            _customerRepository = customerRepository;
            _customerStatusProcessor = customerStatusProcessor;
            _orderDiscountProcessor = orderDiscountProcessor;
        }

        public void ProcessOrder(CustomerEntity customer, OrderEntity order)
        {
            var processingSteps = new List<IProcessStep>()
            {
                new CreateOrUpdateCustomerProcessStep(_customerRepository),
                new CalculateCustomerStatusLevelProcessStep(_customerStatusProcessor),
                new CalculateDiscountProcessStep(_orderDiscountProcessor),
                new SaveOrderProcessStep(),
                new SendEmailWithFinalResultProcessStep()
            };

            foreach (var processingStep in processingSteps)
            {
                processingStep.ExecuteStep(customer, order);
            }
        }
    }
}
