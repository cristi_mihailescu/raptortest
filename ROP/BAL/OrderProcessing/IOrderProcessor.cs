﻿using ROP.DAL;

namespace ROP.BAL
{
    interface IOrderProcessor
    {
        void ProcessOrder(CustomerEntity customer, OrderEntity order);
    }
}
