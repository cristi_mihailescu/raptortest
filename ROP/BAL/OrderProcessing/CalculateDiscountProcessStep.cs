﻿using ROP.DAL;

namespace ROP.BAL
{
    public class CalculateDiscountProcessStep : IProcessStep
    {
        private readonly IOrderDiscountProcessor _orderDiscountProcessor;

        public CalculateDiscountProcessStep(IOrderDiscountProcessor orderDiscountProcessor)
        {
            _orderDiscountProcessor = orderDiscountProcessor;
        }

        public void ExecuteStep(CustomerEntity customer, OrderEntity order)
        {
            _orderDiscountProcessor.ApplyDiscount(customer, order);
        }
    }
}
