﻿using ROP.DAL;

namespace ROP.BAL
{
    public class SaveOrderProcessStep : IProcessStep
    {
        public void ExecuteStep(CustomerEntity customer, OrderEntity order)
        {
            customer.Orders.Add(order);
        }
    }
}
