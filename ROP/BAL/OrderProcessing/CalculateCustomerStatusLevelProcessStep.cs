﻿using ROP.DAL;

namespace ROP.BAL
{
    public class CalculateCustomerStatusLevelProcessStep : IProcessStep
    {
        private ICustomerStatusProcessor _customerStatusProcessor;

        public CalculateCustomerStatusLevelProcessStep(ICustomerStatusProcessor customerStatusProcessor)
        {
            _customerStatusProcessor = customerStatusProcessor;
        }

        public void ExecuteStep(CustomerEntity customer, OrderEntity order)
        {
            _customerStatusProcessor.ProcessCustomerStatus(customer);
        }
    }
}
