﻿using ROP.DAL;

namespace ROP.BAL
{
    interface IProcessStep
    {
        void ExecuteStep(CustomerEntity customer, OrderEntity order);
    }
}
