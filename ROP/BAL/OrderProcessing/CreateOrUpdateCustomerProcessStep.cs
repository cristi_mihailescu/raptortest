﻿using System.Linq;
using ROP.DAL;

namespace ROP.BAL
{
    public class CreateOrUpdateCustomerProcessStep : IProcessStep
    {
        private readonly IRepository<CustomerEntity> _customerRepository;
        public CreateOrUpdateCustomerProcessStep(IRepository<CustomerEntity> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public void ExecuteStep(CustomerEntity customer, OrderEntity order)
        {
            var existingCustomer = _customerRepository.GetAll().FirstOrDefault(c => c.Email == customer.Email);
            if (existingCustomer != null)
            {
                // should I update something here?
                customer = existingCustomer;
                return;
            }
        
            _customerRepository.Save(customer);
        }
    }
}
