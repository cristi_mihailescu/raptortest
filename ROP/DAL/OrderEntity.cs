﻿using System;
using System.Collections.Generic;

namespace ROP.DAL
{
    public class OrderEntity : IEntity
    {
        public long Id { get; set; }

        public IList<OrderProductEntity> Products { get; set; }
        public decimal Total { get; set; }
        public decimal BeforeDiscount { get; set; }
        public decimal Discount { get; set; }
        public DateTime PlacementDate { get; set; }
    }
}
