﻿namespace ROP.DAL
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
