﻿using System.Collections.Generic;
using System.Linq;

namespace ROP.DAL
{
    public class Repository<T> : IRepository<T>
        where T : IEntity
    {
        private readonly IList<T> _entities;

        public Repository()
        {
            _entities = new List<T>();
        }

        public void Save(T item)
        {
            _entities.Add(item);
        }

        public T GetById(long id)
        {
            return _entities.SingleOrDefault(e => e.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return _entities;
        }
    }
}
