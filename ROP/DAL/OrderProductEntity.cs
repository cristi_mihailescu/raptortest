﻿namespace ROP.DAL
{
    public class OrderProductEntity : IEntity
    {
        public long Id { get; set; }
        public ProductEntity Product { get; set; }
        public long Quantity { get; set; }
    }
}
