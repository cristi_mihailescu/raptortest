﻿using System.Collections.Generic;

namespace ROP.DAL
{
    public interface IRepository<T>
        where T : IEntity
    {
        IEnumerable<T> GetAll();
        void Save(T item);
        T GetById(long id);
    }
}
