﻿using System;
using System.Collections.Generic;

namespace ROP.DAL
{
    public class CustomerEntity : IEntity
    {
        public string Name { get; set; }
        public string Adddress { get; set; }
        public string Email { get; set; }

        public long Id { get; set; }

        public CustomerStatus Status { get; set; }
        public DateTime LastStatusUpdate { get; set; }
        public IList<OrderEntity> Orders { get; set; }
    }
}
