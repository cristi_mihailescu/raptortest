﻿namespace ROP.DAL
{
    public class ProductEntity : IEntity
    {
        public string Name { get; set; }
        public long Id { get; set; }
    }
}
