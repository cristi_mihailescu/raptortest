﻿using System;

namespace ROP.DAL
{
    interface IAuditEntity
    {
        DateTime CreateDate { get; set; }
        DateTime UpDateTime { get; set; }
    }
}
