﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROP.BAL;
using ROP.DAL;

namespace UnitTests
{
    [TestClass]
    public class OrderDiscountTests
    {
        [TestMethod]
        public void Test_OrderDiscountProcessor_With_Regular()
        {
            const decimal orderTotal = 101.0M;

            var orderDiscountProcessor = new OrderDiscountProcessor();
            var customer = new CustomerEntity()
            {
                Status = CustomerStatus.Regular
            };
            var order = new OrderEntity()
            {
                Total = orderTotal,
                Discount = 0.0M,
                BeforeDiscount = orderTotal
            };

            orderDiscountProcessor.ApplyDiscount(customer, order);

            Assert.AreEqual(order.BeforeDiscount, order.Total);
            Assert.AreEqual(order.Discount, 0.0M);
        }
        
        [TestMethod]
        public void Test_OrderDiscountProcessor_With_Silver()
        {
            const decimal orderTotal = 101.0M;

            var orderDiscountProcessor = new OrderDiscountProcessor();
            var customer = new CustomerEntity()
            {
                Status = CustomerStatus.Silver
            };
            var order = new OrderEntity()
            {
                Total = orderTotal,
                Discount = 0.0M,
                BeforeDiscount = orderTotal
            };

            orderDiscountProcessor.ApplyDiscount(customer, order);

            Assert.AreNotEqual(order.BeforeDiscount, order.Total);
            Assert.AreEqual(order.Discount, 0.1M * order.BeforeDiscount);
            Assert.AreEqual(order.Total, order.BeforeDiscount - order.Discount);
        }

        [TestMethod]
        public void Test_OrderDiscountProcessor_With_Gold()
        {
            const decimal orderTotal = 101.0M;

            var orderDiscountProcessor = new OrderDiscountProcessor();
            var customer = new CustomerEntity()
            {
                Status = CustomerStatus.Gold
            };
            var order = new OrderEntity()
            {
                Total = orderTotal,
                Discount = 0.0M,
                BeforeDiscount = orderTotal
            };

            orderDiscountProcessor.ApplyDiscount(customer, order);

            Assert.AreNotEqual(order.BeforeDiscount, order.Total);
            Assert.AreEqual(order.Discount, 0.15M * order.BeforeDiscount);
            Assert.AreEqual(order.Total, order.BeforeDiscount - order.Discount);
        }
    }
}
