﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROP.BAL;
using ROP.DAL;

namespace UnitTests
{
    [TestClass]
    public class CustomerStatusTests
    {
        [TestMethod]
        public void Test_CustomerStatusProcessor_To_Silver()
        {
            var customerStatusProcessor = new CustomerStatusProcessor();

            var customer = new CustomerEntity
            {
                Status = CustomerStatus.Regular,
                Orders = new List<OrderEntity>()
                {
                    new OrderEntity()
                    {
                        Total = 150M
                    },
                    new OrderEntity()
                    {
                        Total = 250M
                    }
                },
                LastStatusUpdate = DateTime.UtcNow.AddDays(-8)
            };

            customerStatusProcessor.ProcessCustomerStatus(customer);

            Assert.AreEqual(customer.Status, CustomerStatus.Silver);
        }

        [TestMethod]
        public void Test_CustomerStatusProcessor_To_Gold()
        {
            var customerStatusProcessor = new CustomerStatusProcessor();

            var customer = new CustomerEntity
            {
                Status = CustomerStatus.Silver,
                Orders = new List<OrderEntity>()
                {
                    new OrderEntity()
                    {
                        Total = 150M
                    },
                    new OrderEntity()
                    {
                        Total = 250M
                    },
                    new OrderEntity()
                    {
                        Total = 250M
                    }
                },
                LastStatusUpdate = DateTime.UtcNow.AddDays(-8)
            };

            customerStatusProcessor.ProcessCustomerStatus(customer);

            Assert.AreEqual(customer.Status, CustomerStatus.Gold);
        }
    }
}
